cmake_minimum_required(VERSION 3.5)
project(osp_naive)

find_package(ament_cmake REQUIRED)
ament_package()
